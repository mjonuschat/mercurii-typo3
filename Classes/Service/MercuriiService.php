<?php

namespace MojoCode\Mercurii\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013-2015 Morton Jonuschat <m.jonuschat@mojocode.de>, MoJo Code
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use MojoCode\Mercurii\Domain\Model\Subscriber;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MercuriiService implements SingletonInterface
{
    const USER_AGENT = 'Mercurii TYPO3 Extension';

    /**
     * Perform a new subscription
     *
     * @param \MojoCode\Mercurii\Domain\Model\Subscriber $subscriber
     * @param string $formUrl
     * @return array
     */
    public function subscribe(Subscriber $subscriber, $formUrl)
    {
        $data = http_build_query([
            'contact' => [
                'email' => $subscriber->getEmail(),
                'name' => $subscriber->getName(),
            ],
        ]);

        $ch = $this->getCurlHandle();
        curl_setopt($ch, CURLOPT_URL, $formUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_exec($ch);
        return curl_getinfo($ch);
    }

    /**
     * @return resource
     * @throws \TYPO3\CMS\Core\Package\Exception
     */
    protected function getCurlHandle()
    {
        $userAgentString = MercuriiService::USER_AGENT . '/' . ExtensionManagementUtility::getExtensionVersion('mercurii');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgentString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'X-Forwarded-For: ' . GeneralUtility::getIndpEnv('REMOTE_ADDR'),
            'Referer: ' . $this->getRefererUrl()
        ]);

        return $ch;
    }

    /**
     * @return string
     */
    protected function getRefererUrl()
    {
        return substr(
            GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'),
            0,
            strlen(GeneralUtility::getIndpEnv('QUERY_STRING')) * -1
        );
    }
}
