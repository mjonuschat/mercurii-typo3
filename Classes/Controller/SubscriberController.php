<?php

namespace MojoCode\Mercurii\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013-2015 Morton Jonuschat <m.jonuschat@mojocode.de>, MoJo Code
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use MojoCode\Mercurii\Domain\Model\Subscriber;
use MojoCode\Mercurii\Service\MercuriiService;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class SubscriberController extends ActionController
{
    /**
     * @var \MojoCode\Mercurii\Service\MercuriiService
     */
    protected $mercuriiService;

    /**
     * Inject the mercuriiService
     *
     * @param \MojoCode\Mercurii\Service\MercuriiService $mercuriiService
     */
    public function injectMercuriiService(MercuriiService $mercuriiService)
    {
        $this->mercuriiService = $mercuriiService;
    }

    /**
     * action new.
     *
     * @param \MojoCode\Mercurii\Domain\Model\Subscriber $newSubscriber
     * @dontvalidate $newSubscriber
     */
    public function newAction(Subscriber $newSubscriber = null)
    {
        $this->view->assign('newSubscriber', $newSubscriber);
    }

    /**
     * action create.
     *
     * @param \MojoCode\Mercurii\Domain\Model\Subscriber $newSubscriber
     */
    public function createAction(Subscriber $newSubscriber)
    {
        $result = $this->mercuriiService->subscribe($newSubscriber, $this->settings['formUrl']);
        $this->view->assign('result', $result);
    }
}
