<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "mercurii"
 *
 * Auto generated by Extension Builder 2013-03-25
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Mercurii Newsletter Subscription',
    'description' => 'The Mercurii Newsletter Subscription plugin for TYPO3 provides a subscription interface to gather subscriber data.',
    'category' => 'plugin',
    'author' => 'Morton Jonuschat',
    'author_email' => 'm.jonuschat@mojocode.de',
    'author_company' => 'MoJo Code',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '0.9.1',
    'constraints' => [
        'depends' => [
            'php' => '5.4.0-5.6.99',
            'extbase' => '6.2.0-7.99.99',
            'fluid' => '6.2.0-7.99.99',
            'typo3' => '6.2.0-7.99.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
