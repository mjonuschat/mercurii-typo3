<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'MojoCode.' . $_EXTKEY,
    'Main',
    [
        'Subscriber' => 'new, create',

    ],
    // non-cacheable actions
    [
        'Subscriber' => 'create',

    ]
);
