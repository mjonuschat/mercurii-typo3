<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Main',
    'LLL:EXT:mercurii/Resources/Private/Language/locallang_db.xlf:plugin.main'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['mercurii_main'] = 'layout,select_key,recursive,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['mercurii_main'] = 'pi_flexform';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'mercurii_main',
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/Main.xml'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'Mercurii Newsletter Subscription'
);
