plugin.tx_mercurii {
	view {
		templateRootPaths {
			100 = EXT:mercurii/Resources/Private/Templates/
		}

		partialRootPaths {
			100 = EXT:mercurii/Resources/Private/Partials/
		}

		layoutRootPaths {
			100 = EXT:mercurii/Resources/Private/Layouts/
		}
	}
}

page.includeJSFooterlibs {
	mailcheck = EXT:mercurii/Resources/Public/JavaScripts/mailcheck.js
}

page.jsFooterInline {
	65663 = COA
	65663 {
		10 = TEXT
		10.value (
		document.addEventListener("DOMContentLoaded", function() {
			var mailcheckInputs = document.querySelectorAll('.js-mercurii-mailcheck');
			[].forEach.call(mailcheckInputs, function(input) {
				input.addEventListener('change', function(event) {
					var suggestionBox = this.parentNode.querySelector('.js-mercurii-mailcheck-suggestion');
					var result = Mailcheck.run({
						email: this.value,
						domains: [{$plugin.tx_mercurii.settings.domains}],
						topLevelDomains: [{$plugin.tx_mercurii.settings.topLevelDomains}],
					});

					suggestionBox.innerHTML = result
						? suggestionBox.getAttribute('data-message') + ' <b>' + result.full + '</b>'
						: '';
				});
			});
		});
		)
	}
}
